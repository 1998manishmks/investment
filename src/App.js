import React from 'react';
import {makeStyles} from '@material-ui/core';
import Homepage from './Pages/HomePage';
import './App.css';
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import CoinPage from './Pages/CoinPage';
import PageNotFound from './Pages/404Page';
import Header from './components/Header';

const useStyles = makeStyles(() => ({
  App: {
    backgroundColor: '#14161a',
    color: 'white',
    minHeight: '100vh',
  },
}));

function App() {
  const classes = useStyles();

  return (
    <BrowserRouter>
      <div className={classes.App}>
        <Header />
        <Routes>
          <Route path="/" element={<Homepage/>} />

          {/*
            added only for gitlab deployment purposes
            because there the path start with investment
          */}
          <Route path="/investment" element={<Homepage/>} />

          <Route path="/coins/:id" element={<CoinPage/>} />
          <Route path="*" element={<PageNotFound/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
