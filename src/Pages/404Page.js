import React from 'react';
import {makeStyles} from '@material-ui/core';
import {Link} from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: '20vh',
    padding: 40,
  },
  info: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    lineHeight: '300%',
  },
//   [theme.breakpoints.down('md')]: {
//     width: '100%',
//     marginTop: 0,
//     padding: 20,
//     paddingTop: 0,
//     flexDirection: 'rows',
//   },
}));

const PageNotFound = () => {
  const classes = useStyles();
  //   const navigate = useNavigate();

  return (
    <div className={classes.container}>
      <img src="/404.jpeg" height="400px"/>
      <div className={classes.info}>
        <h1>AWWW...DON’T CRY.</h1>
        <p>Its just a 404 Error!</p>
        <p>What you’re looking for may have been misplaced
            in Long Term Memory.</p>
        <Link to="/">
          <p>Go Back</p>
        </Link>
      </div>
    </div>
  );
};

export default PageNotFound;
