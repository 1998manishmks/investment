const baseURL = 'https://api.coingecko.com/api/v3/coins/';

export const coinList = (currency) =>
  `${baseURL}markets?vs_currency=${currency}` +
  `&order=market_cap_desc&per_page=100&page=1&sparkline=false`;

export const singleCoin = (id) =>
  `${baseURL}${id}`;

export const historicalChart = (id, days = 365, currency) =>
  `${baseURL}${id}/market_chart?vs_currency=${currency}&days=${days}`;

export const trendingCoins = (currency) =>
  `${baseURL}markets?vs_currency=${currency}&order=gecko_desc&` +
 `per_page=10&page=1&sparkline=false&price_change_percentage=24h`;
