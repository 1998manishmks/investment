FROM alpine:3.13
RUN apk add --update npm
WORKDIR /opt
COPY ./package.json .
RUN npm install
COPY . .
EXPOSE 3000
CMD ["npm", "start"]